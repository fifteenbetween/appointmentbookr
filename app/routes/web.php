<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/calendar');
});
Route::prefix('/calendar')->group(function () {
    Route::get('/',             [App\Http\Controllers\CalendarController::class, 'index']);
    Route::get('/load/index',   [App\Http\Controllers\CalendarController::class, 'indexContent']);
    Route::get('/load/times',   [App\Http\Controllers\CalendarController::class, 'timeSlots']);
    Route::post('/book',        [App\Http\Controllers\CalendarController::class,  'book']);
});
