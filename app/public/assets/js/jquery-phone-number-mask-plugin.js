(function($) {

    $.fn.phoneMasker = function(){
        $(this).click(function(){
            let phoneNumber = $.trim( $(this).text());
            if(phoneNumber.length === 0)
            {
                return $(this).val('(');
            }
        });

        $(this).keyup(function (evt) {
            let phoneNumberStr = $.trim( $(this).val() );
            let count = phoneNumberStr.split("(").length;

            //ENSURE AFTER BACKSPACE THAT ONLY 1 BEGINNING PARENTHESES IS ADDED
            if(count > 2)
            {
                return $(this).val('(');
            }
            //IF KEYCODE DOES NOT EQUAL 8 DONT RUN THE CODE
            if(evt.keyCode !== 8)
            {
                //PREVENTS USER FROM GOING OVER THE CHARACTER LIMIT
                if(phoneNumberStr.length > 14 ){
                    return $(this).val(phoneNumberStr.slice(0,-1));
                }
                if(phoneNumberStr.length === 1 ){
                    return $(this).val("("+ $(this).val());
                }
                if(phoneNumberStr.length === 4 ){
                    return $(this).val($(this).val()+") ");
                }
                if(phoneNumberStr.length === 9 ){
                    return $(this).val($(this).val()+"-");
                }
            }
        });
        return this.val();
    };
} ( jQuery ) );
