$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
});
$(function()
{

    __ShowCalendar();
    function __ShowCalendar(){
        $.get("/calendar/load/index", function(data) {
        }).done(function(data) {
            $("#showCalender").html(data);
        }).fail(function() {
            alert('Connection failure');
        });
    }

    //DECREMENT CALENDAR DATE
    $(document).on('click','#minusDate',function () {
        let dateId = $("#calendarTitle").attr("data");
        $.get('/calendar/load/index',{'action':'sub','id':dateId},function(data){
        }).done(function(data) {
            $("#showCalender").html(data);
            $("#showTimeslot").html("");
        }).fail(function() {
            alert('Connection failure');
        });
    });

    //INCREMENT CALENDAR DATE
    $(document).on('click','#addDate',function () {
        let dateId = $("#calendarTitle").attr("data");
        $.get('/calendar/load/index',{'action':'add','id':dateId},function(data){
        }).done(function(data) {
            $("#showCalender").html(data);
            $("#showTimeslot").html("");
        }).fail(function() {
            alert('Connection failure');
        });
    });

    //SELECTING CALENDAR DATE
    $(document).on('click','.calender_date',function () {
        $('.calender_date').removeClass("selected");
        $(this).addClass("selected");
        let dateS = $(this).attr("id");
        $.get('/calendar/load/times',{'id':dateS},function(_){
        }).done(function (data) {
            $("#showTimeslot").html(data);
        }).fail(function () {
            $("#showTimeslot").html('Connection failure');
        });
    });

    $(document).on('click','#btnReserve',function () {
        let sDate = $(".selected").attr("id");
        let sTime = $("input[name=time-slots]:checked").val();

        if(!sTime || sTime.length === 0){
            alert('Please select a time');
            return;
        }
        //ADD SELECT TIME/DATE TO CONFIRM FORM
        $("[name=date_selected]").val(sDate);
        $("[name=time_selected]").val(sTime);

        //SHOW CONFIRM RESERVATION MODAL
        showConfirmReservationModal();
    });

    //CONFIRM RESERVATION MODAL
    function showConfirmReservationModal() {
        $('#confirmReservationModal').modal({
            backdrop: 'static',
            keyboard: true,
            show: true
        });
    }
    function closeModal(){
        let formId = $(".modal form").attr("id");
        $('#'+formId).trigger("reset"); //reset form
        $(".modal").modal('hide'); // close form
    }

    //SUBMIT CONFIRMATION FORM
    $(document).on('click','#btnConfirmReserve',function (e) {
        e.preventDefault();
        $.post('/calendar/book', $.param( $("#customerBookingForm :input").serializeArray() ),function (data) {
        }).done(function(data) {
            if(data.status === "F"){
                alert(data.message);
            }else{
                alert(data.message);
                closeModal();
            }
        }).fail(function() {
            alert('Connection failure');
        });
    });
    $("#inputPhoneNumber").phoneMasker();
});
