<?php


namespace App\Libs;

use App\Models\BookedTimeSlots;
use App\Models\OperatingHour;

class BuildTimeMatrix
{
    private $operatingHour;
    private $bookedTimeSlots;

    public function __construct(OperatingHour $operatingHour, BookedTimeSlots $bookedTimeSlots)
    {
        $this->operatingHour = $operatingHour;
        $this->bookedTimeSlots = $bookedTimeSlots;
    }
    public function availableTimes($userId, $dateStamp) :string
    {

        // LIST OF TIMES TO BLOCK OUT FROM LIST
        $block_times =  $this->bookedTimeSlots->getBookedTimeSlots($dateStamp);

        /* ========== OPERATING HOUR PARAMS =========== */
        /* IF NO OPERATING HOURS IS SET. DEFAULTS TO 00:00 - 23:00 */
        $operatingHour  = $this->operatingHour->getOperatingHour($userId);
        $opening_hrs    = $operatingHour['opening_hr'];
        $closing_hrs    = $operatingHour['closing_hr'];
        $interval       = $operatingHour['interval'];

        $x = 0;
        $timeCol = array();

        // SETS OPENING HRS AND CLOSING HRS RANGE
        while( strtotime($opening_hrs) <  strtotime($closing_hrs) )
        {

            $makeTime  = date("H:i", strtotime("+{$x} minutes". $opening_hrs)   );

            $timeCol[] = $makeTime;
            $x += $interval;

            if(strtotime($makeTime) >= strtotime($closing_hrs) ):
                break;
            endif;
        }
        // IF BLOCKED TIME IS IN AVAILABLE TIME SLOTS LIST, REMOVE
        foreach ($block_times as $block_time)
        {
            if( in_array($block_time['times'], $timeCol) ):
                $key = array_search($block_time, $timeCol);
                unset( $timeCol[$key] );
            endif;
        }

        // NEW LIST OF AVAILABLE TIME SLOTS MINUS BLOCKED TIMES
        $bookingTimes = $timeCol;

        //HTML TABLE & DIV OPEN
        $tableResponsiveOpen = '<div class="table-responsive" id="time-available">';
        $tableOpen = '<table class="table table-bordered text-center">';

        $table ='';
        $table .='<tr>';
        $table .='<td class="text-center" colspan="4">Available Time Slots</td>';
        $table .='</tr>';
        $table .='<tr>';

        $cnt = 1;
        foreach($bookingTimes as $times):
            $table .= '<td><input type="radio" class="mr-1" name="time-slots" value="'.date("H:i", strtotime($times)).'">'. date("g:i A", strtotime($times)) .'</td>';
            if($cnt === 4):
                $cnt = 0;
                $table .= '</tr><tr>';
            endif;
            $cnt++;
        endforeach;

        $table .= '</tr>';

        //HTML TABLE & DIV CLOSE
        $tableClose = '</table>';
        $tableResponsiveClose = '</div>';

        $button = "";
        $button .= '<div class="mb-3">';
        $button .= '<button class="btn btn-success btn-block btn-lg rounded-0" id="btnReserve">Reserve</button>';
        $button .= '</div>';

        return $tableResponsiveOpen . $tableOpen . $table. $tableClose . $tableResponsiveClose . $button;

    }
}
