<?php


namespace App\Libs;


use App\Models\OperatingDay;
use App\Models\BlockedDate;
use Log;
class BuildCalender
{
    private $operatingDay;
    private $blockedDate;
    public function __construct(OperatingDay $operatingDay,BlockedDate $blockedDate)
    {
        $this->operatingDay = $operatingDay;
        $this->blockedDate  = $blockedDate;
    }

    public function buildCalender($userId, $calYear, $calMonth) : string
    {

        $date_string        = $calYear."-". sprintf('%02d', $calMonth) ."-01";
        $date_start         = date("w", strtotime($date_string));   //numeric representation of the day
        $days_in_month      = date("t", strtotime($date_string));   //number of days in the given month
        $month_year_string  = date("M Y", strtotime($date_string)); // month, year for calendar title

        //CALENDAR MATRIX DAYS OF THE WEEK HEADERS
        $day_headers = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];

        //POSSIBLE NUMBER OF CALENDAR BLOCKS. USE TO CALCULATE TABLE SPACE FILLER
        $maxSpaces = 35;       // NORMAL NUMBER OF CALENDAR BLOCKS.
        if($date_start >= 5):
            $maxSpaces = 42;   // ABNORMAL NUMBER OF CALENDAR BLOCKS.
        endif;

        $tdDateFiller = ( ($maxSpaces - $days_in_month) - $date_start);

        $calTitle = "";
        $calDays  = "";
        $calDates = "";

        //HTML TABLE & DIV OPEN
        $tableResponsiveOpen = '<div class="table-responsive mb-0">';
        $tableOpen = '<table class="table table-bordered text-center">';

        //CALENDAR MONTH & YEAR HEADER
        $calTitle .='<tr>';
        $calTitle .='<td id="minusDate" class="finger">-</td>';
        $calTitle .='<td colspan="5" id="calendarTitle" data="'.date("Y-m-d", strtotime($month_year_string)).'">'. $month_year_string .'</td>';
        $calTitle .='<td id="addDate" class="finger">+</td>';
        $calTitle .='</tr>';

        //CALENDAR DAYS HEADER
        $calDays .='<tr>';
        for($x=0; $x < count($day_headers); $x++):
            $calDays .= '<th>'.$day_headers[$x].'</th>';
        endfor;
        $calDays .='</tr>';

        //CONCATENATE TABLE HTML, CALENDAR HEADER, CALENDAR LABELS
        $calDates .= $tableResponsiveOpen . $tableOpen . $calTitle . $calDays;

        $calDates .='<tr>';

        //START CALENDAR DAY FILLER
        for($x=0; $x < $date_start; $x++):
            $calDates .='<td class="calender_filler">&nbsp;</td>';
        endfor;


        for($z = 1; $z <= $days_in_month; $z++):
            $date_start = $date_start+1;

            $calender_date_id = date("Y-m-d", mktime(0,0,0,$calMonth,$z,$calYear)  );

            $calDates .='<td class="';

            /* FILTER THROUGH CALENDAR DATES. BUILDING LIST OF CSS CLASSES TO ADD TO CALENDAR DATE CELL */
            //$calDates .= $this->calendarDateFilter($calender_date_id);
            $calDates .=DateFilters::calendarDateFilter($calender_date_id, $this->blockedDate->getBlockedOutDates($userId), $this->operatingDay->getOperatingDays($userId));
            /* ----------------------------- */

            $calDates .='"  id="'.$calender_date_id.'"><div class="date-container">'.$z.'</div></td>';
            if($date_start === 7):
                $date_start = 0;
                $calDates .= '</tr><tr>';
            endif;
        endfor;

        //END CALENDAR DAY FILLER
        for($x=0; $x < $tdDateFiller; $x++):
            $calDates .='<td class="calender_filler">&nbsp;</td>';
        endfor;

        $calDates .= '<tr>';

        //HTML TABLE & DIV CLOSE
        $tableClose = '</table>';
        $tableResponsiveClose = '</div>';

        //CONCATENATE CALENDAR DATES, TABLE HTML
        return $calDates.$tableClose.$tableResponsiveClose;
    }
}
