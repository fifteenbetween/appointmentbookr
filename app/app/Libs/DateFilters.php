<?php


namespace App\Libs;

class DateFilters
{
    public static function calendarDateFilter($calenderDate, $blockedOutDates, $operatingDays) : string
    {

        $calDates = "";

        if( in_array($calenderDate, $blockedOutDates ) ):
            $calDates .= 'block_out';
        endif;

        if( !in_array($calenderDate, $blockedOutDates ) && in_array( date("w", strtotime($calenderDate )) , $operatingDays ) ):
            $calDates .= ' calender_date';
        endif;

        if($calenderDate === date("Y-m-d", time()) ):
            $calDates .= ' calender_today';
        endif;

        if( !in_array( date("w", strtotime($calenderDate )) , $operatingDays ) ):
            $calDates .= ' offday';
        endif;

        return $calDates;
    }
}
