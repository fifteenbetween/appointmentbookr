<?php

namespace App\Http\Controllers;

use App\Libs\BuildCalender;
use App\Libs\BuildTimeMatrix;
use App\Models\AppointmentBooking;
use Illuminate\Http\Request;
class CalendarController extends Controller
{
    private $userId;
    private $buildCalender;
    private $buildTimeMatrix;
    public function __construct(BuildCalender $buildCalender, BuildTimeMatrix $buildTimeMatrix)
    {
        $this->buildCalender = $buildCalender;
        $this->buildTimeMatrix = $buildTimeMatrix;

        $this->userId = 1;
    }

    public function index(){
        return view('pages.calendar');
    }

    public function indexContent(Request $request){

        $get = $request->all();

        if( !array_key_exists('action', $get) ||  !array_key_exists('id', $get) ):
            $action     = null;
            $dateStamp  = date("Y-m-d", time() );
        else:
            $action     = filter_var($get['action'], FILTER_SANITIZE_STRING);
            $dateStamp  = filter_var($get['id'], FILTER_SANITIZE_STRING);
        endif;

        if($action ==='sub'):
            $dateStamp = date("Y-m-d", strtotime("-1 month".$dateStamp));
        endif;

        if($action ==='add'):
            $dateStamp = date("Y-m-d", strtotime("+1 month".$dateStamp));
        endif;

        //PARSE TIME TO FOR YEAR AND MONTH;
        $pTime = date_parse($dateStamp);

        $calYear  = $pTime['year'];
        $calMonth = $pTime['month'];

        $calender = $this->buildCalender->buildCalender($this->userId, $calYear,$calMonth);

        return response($calender);
    }

    public function timeSlots(Request $request){
        $get = $request->all();

        if( !array_key_exists('id', $get) ){
            $dateStamp  = date("Y-m-d", time() );
        } else {
            $dateStamp  = filter_var($get['id'], FILTER_SANITIZE_STRING);
        }

        $availableTimesMatrix = $this->buildTimeMatrix->availableTimes($this->userId, $dateStamp);
        return response($availableTimesMatrix);
    }

    public function book(Request $request){
        $post = $request->all();

        $required_form_keys = [
            'date_selected' =>'Date',
            'time_selected' =>'Time',
            'first_name'    =>'First Name',
            'last_name'     =>'Last Name',
            'email'         =>'Email',
            'phone_number'  =>'Phone'
        ];
        foreach ($required_form_keys as $k=>$v){
            if( !array_key_exists($k, $post) ){
                return response()->json(['status'=>'F', 'message'=>'"'.$v.'" is a required field.', 'data'=>'']);
            }
        }
        foreach ($required_form_keys as $k=>$v){
            if( is_null( $post[$k] ) ){
                return response()->json(['status'=>'F', 'message'=>'"'.$v.'" is a required field.', 'data'=>'']);
            }
        }

        // BUILD DATA ARRAY TO SAVE TO TABLE
        foreach ($required_form_keys as $k=>$v){
            $data[$k] = filter_var($post[$k], FILTER_SANITIZE_STRING);
        }

        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            return response()->json(['status'=>'F', 'message'=>'Invalid email address.', 'data'=>'']);
        }

        //ADD USER ID TO BOOKING DATA ARRAY TO BE SAVED
        $data['user_id'] = $this->userId;

        $appointmentBooking = new AppointmentBooking();
        $appointmentBookingStatus = $appointmentBooking->saveAppointmentBooking($data);

        if($appointmentBookingStatus->status != "T"){
            return response()->json(['status'=>'F', 'message'=>'Update failure, transaction incomplete.', 'data'=>'']);
        }

        //SEND NOTICE TO ADMIN, RETURN STATUS TO JS
        return response()->json(['status'=>'T', 'message'=>'Thank you! We will be in touch shortly.', 'data'=>'']);
    }
}
