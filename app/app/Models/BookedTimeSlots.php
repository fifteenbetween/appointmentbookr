<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookedTimeSlots extends Model
{
    use HasFactory;

    public function getBookedTimeSlots($datestamp = null): array{
        if(is_null($datestamp)){
            return [];
        }
        $bookedTimeSlots  = BookedTimeSlots::select('times')->where('datestamp', $datestamp)->get();
        if(!$bookedTimeSlots){
            return [];
        }
        return collect($bookedTimeSlots)->toArray();
    }
}
