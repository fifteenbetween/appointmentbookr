<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppointmentBooking extends Model
{
    use HasFactory;
    protected $fillable = ['user_id', 'date_selected', 'time_selected', 'first_name', 'last_name', 'email'];
    public function saveAppointmentBooking($data) : object{
        try {
            $createStatus = AppointmentBooking::create($data);

            if((string)$createStatus->exists === "1" || (string)$createStatus->wasRecentlyCreated === "1"){
                return (object) ['status'=>'T', 'message'=>'Update Successful', 'data'=>$createStatus->id];
            }else{
                return (object) ['status'=>'F', 'message'=>'Update Unsuccessful!', 'data'=>0];
            }
        } catch(\Illuminate\Database\QueryException $e) {
            $errorCode = $e->errorInfo[1];
            return (object) ['status'=>'F', 'message'=>$errorCode, 'data'=>0];
        }
    }
}
