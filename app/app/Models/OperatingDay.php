<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperatingDay extends Model
{
    use HasFactory;

    public function getOperatingDays($userId = null): array {
        if(is_null($userId)){
            return [];
        }
        $operatingDay  = OperatingDay::select('workdays')->where('user_id', $userId)->first();
        if(!$operatingDay){
            return [];
        }
        //CONVERT JSON TO OPERATING DAYS ARRAY
        return json_decode($operatingDay->workdays, true);
    }

}
