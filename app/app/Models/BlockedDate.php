<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlockedDate extends Model
{
    use HasFactory;

    public function getBlockedOutDates($userId = null) : array{
        if(is_null($userId)){
            return [];
        }
        $blockedDates  = BlockedDate::select('block_date')->where('user_id', $userId)->get();
        if(!$blockedDates){
            return [];
        }
        $map = $blockedDates->map(function($items){
            return $items->block_date;
        });
        return $map->toArray();
    }

}
