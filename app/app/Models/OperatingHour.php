<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class OperatingHour extends Model
{
    use HasFactory;

    public function getOperatingHour($userId = null) : array{
        if(is_null($userId)){
            return [];
        }
        $operatingHour  = OperatingHour::select('opening_hr', 'closing_hr','interval')->where('user_id', $userId)->first();
        if(!$operatingHour){
            return [];
        }
        $data['opening_hr'] = $operatingHour->opening_hr;
        $data['closing_hr'] = $operatingHour->closing_hr;
        $data['interval']   = $operatingHour->interval;
        return $data;
    }
}
