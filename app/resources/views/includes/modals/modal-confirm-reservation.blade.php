<div class="modal" id="confirmReservationModal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content rounded-0">
            <div class="modal-header">
                <h5 class="modal-title">Confirm Reservation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="customerBookingForm">
                    <p>Greeting message goes here. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam asperiores, assumenda
                        dignissimos inventore natus, nemo nobis omnis porro praesentium quae, sapiente sit tempore.
                        Pariatur, qui.</p>
                    <input type="hidden" name="date_selected">
                    <input type="hidden" name="time_selected">
                    <div class="form-group">
                        <input class="form-control form-control-lg" id="inputFirstname" name="first_name" type="text" placeholder="First name" required>
                    </div>

                    <div class="form-group">
                        <input class="form-control form-control-lg" id="inputLastname" name="last_name" type="text" placeholder="Last name" required>
                    </div>

                    <div class="form-group">
                        <input class="form-control form-control-lg" id="inputEmail" name="email" type="email" placeholder="Email address" required>
                    </div>

                    <div class="form-group">
                        <input class="form-control form-control-lg" id="inputPhoneNumber" name="phone_number" type="phone" placeholder="Phone number" required oninput="this.value = this.value.replace(/[^0-9\-()\s]/g, '').replace(/(\..*)\./g, '$1');">
                    </div>
                    <button class="btn btn-success btn-block btn-lg rounded-0" id="btnConfirmReserve">Confirm Reservation</button>
                </form>

            </div>
            <div class="modal-footer" style="justify-content:flex-start !important;">
                <p><small>Disclaimer message goes here.<br>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam asperiores, assumenda</small></p>
            </div>
        </div>
    </div>
</div><!--//modal-->

