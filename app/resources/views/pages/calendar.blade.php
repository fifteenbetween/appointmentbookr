@extends('layouts.app-main')
@section('title', 'Appointment Booker')
@section('description', 'Appointment Booker')
@section('keywords', 'Appointment Booker')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-5 col-12 mx-auto pt-5">
            <span id="showCalender"></span>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-12 mx-auto">
            <span id="showTimeslot"></span>
        </div>
    </div>
</div>

<!-- Modal-->
@include('includes.modals.modal-confirm-reservation')
<!-- //Modal-->

@endsection
