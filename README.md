# README #

Sample Laravel application for Little Rhino viewing

### What is this repository for? ###

* Appointment Bookr

_Current version has no admin control panel_

## TABLE NAMES AND EXPLANATION
* app/calendars_db_2021-04-19.sql
* appointment_bookings - CONTAINS CUSTOMER BOOKING DATA
* blocked_dates - SPECIFIC DATES BLOCKED BY ADMIN
* booked_time_slots - DATES AND TIMES DAYS BLOCKED BY ADMIN. FOR BOOKINGS AFTER ADMIN CUSTOMER VERIFIES CUSTOMER REQUEST.
* operating_days - BUSINESS OPEN DAYS. I.E. BUSINESS IS OPEN M-F (1-5)
* operating_hours  - BUSINESS OPEN HOURS. OPEN, CLOSE TIMES. INTERVAL EQUAL MINUTES APART. I.E. CUSTOM CAN BOOK BETWEEN 08:00-17:30, 30 MINUTES BETWEEN EACH BOOKING (8:00AM-5:30PM)

![Screen Shot of Appointment Bookr](/appointmentbookr_screenshot.png?raw=true "Screen Shot of Appointment Bookr")
