# ************************************************************
# Sequel Ace SQL dump
# Version 3028
#
# https://sequel-ace.com/
# https://github.com/Sequel-Ace/Sequel-Ace
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.18-MariaDB)
# Database: calendars_db
# Generation Time: 2021-04-19 17:42:33 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE='NO_AUTO_VALUE_ON_ZERO', SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table appointment_bookings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `appointment_bookings`;

CREATE TABLE `appointment_bookings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `date_selected` date DEFAULT NULL,
  `time_selected` varchar(5) DEFAULT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `email` varchar(75) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table blocked_dates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `blocked_dates`;

CREATE TABLE `blocked_dates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `block_date` date DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `blocked_dates` WRITE;
/*!40000 ALTER TABLE `blocked_dates` DISABLE KEYS */;

INSERT INTO `blocked_dates` (`id`, `user_id`, `block_date`, `updated_at`, `created_at`)
VALUES
	(1,1,'2021-04-23','2021-04-19 17:31:56','2021-04-19 17:31:56');

/*!40000 ALTER TABLE `blocked_dates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table booked_time_slots
# ------------------------------------------------------------

DROP TABLE IF EXISTS `booked_time_slots`;

CREATE TABLE `booked_time_slots` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `datestamp` date DEFAULT NULL,
  `times` varchar(5) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `booked_time_slots` WRITE;
/*!40000 ALTER TABLE `booked_time_slots` DISABLE KEYS */;

INSERT INTO `booked_time_slots` (`id`, `user_id`, `datestamp`, `times`, `updated_at`, `created_at`)
VALUES
	(1,1,'2021-04-22','14:00','2021-04-19 17:31:56','2021-04-19 17:31:56');

/*!40000 ALTER TABLE `booked_time_slots` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table operating_days
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operating_days`;

CREATE TABLE `operating_days` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `workdays` text DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `operating_days` WRITE;
/*!40000 ALTER TABLE `operating_days` DISABLE KEYS */;

INSERT INTO `operating_days` (`id`, `user_id`, `workdays`, `updated_at`, `created_at`)
VALUES
	(1,1,'[\"1\",\"2\",\"3\",\"4\",\"5\"]','2021-04-19 17:31:56','2021-04-19 17:31:56');

/*!40000 ALTER TABLE `operating_days` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table operating_hours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `operating_hours`;

CREATE TABLE `operating_hours` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `opening_hr` varchar(5) DEFAULT NULL,
  `closing_hr` varchar(5) DEFAULT NULL,
  `interval` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `operating_hours` WRITE;
/*!40000 ALTER TABLE `operating_hours` DISABLE KEYS */;

INSERT INTO `operating_hours` (`id`, `user_id`, `opening_hr`, `closing_hr`, `interval`)
VALUES
	(1,1,'08:00','17:30','30');

/*!40000 ALTER TABLE `operating_hours` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
